﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITargetDetectionSystem
{
    void EnableScan();
    void DisableScan();

    int GetMaxSimultaneousTargetLimit();
    float GetDetectionRange();
    string GetDetectionType();
    List<GameObject> GetDetectedTargets();

}
