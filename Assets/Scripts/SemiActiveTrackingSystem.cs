﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SemiActiveTrackingSystem : TargetTrackingSystem
{
    public RadarSystem baseRadarSystem;

    // Use this for initialization
    void Start()
    {
        base.Start();
        InvokeRepeating("Track", 0.0f, baseRadarSystem.scanInterval);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Track()
    {
        foreach(var target in baseRadarSystem.targets)
        {
            if(this.target == target.transform)
            {
                tracking = true;
                return;
            }
        }

        tracking = false;
    }
}
