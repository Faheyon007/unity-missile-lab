﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class TargetTrackingSystem : MonoBehaviour
{
    public float maneuverability = 10.0f;    // degree/s

    public Transform target;

    public Rigidbody rigidbody;

    public bool tracking = false;

    // Use this for initialization
    virtual public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void FixedUpdate()
    {
        if (tracking)
        {
            rigidbody.MoveRotation(Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, target.position - transform.position, maneuverability * Time.deltaTime * Mathf.Deg2Rad, 0.0f)));
        }
    }

    void Track()
    {
        if (tracking)
        {
            rigidbody.MoveRotation(Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, target.position - transform.position, maneuverability * Time.deltaTime * Mathf.Deg2Rad, 0.0f)));
        }
    }
}
