﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class WarheadDetonationSystem : MonoBehaviour
{
    public float explosionForce = 100.0f;   // N
    public float explosionVelocity = 8000.0f;   // m/s
    public float weight = 50.0f;    // kg
    public float blastRadius = 10.0f;

    public float detonationTriggerDistance = 10.0f;

    public Rigidbody rigidbody;
    public TargetTrackingSystem targetTrackingSystem;

    public GameObject exlopsionGameObject;

    public bool detonationEnabled = true;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        targetTrackingSystem = GetComponent<TargetTrackingSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, targetTrackingSystem.target.transform.position) <= detonationTriggerDistance)
        {
            Detonate();
        }
    }

    public void Detonate()
    {
        if (detonationEnabled)
        {
            var colliders = Physics.OverlapSphere(transform.position, detonationTriggerDistance);

            foreach (var collider in colliders)
            {
                try
                {
                    collider.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, blastRadius);
                }
                catch (Exception e)
                {
                }
            }

            Destroy(this.gameObject);
        }
    }
}
