﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
class HeatBasedTargetDetectionSystem : MonoBehaviour, ITargetDetectionSystem
{
    public float detectionRange = 1000; // meters
    public int maxSimultaneousTargetLimit = 10;

    public bool scanEnabled = false;
    public float scanInterval = 0.5f;

    public float heatSensitivity = 0.5f;


    public List<GameObject> targets = new List<GameObject>();



    void Start()
    {

    }

    void Update()
    {

    }

    void FixedUpdate()
    {

    }


    public List<GameObject> GetDetectedTargets()
    {
        List<GameObject> emitters = GetHeatEmiitters();
        List<GameObject> newTargets = new List<GameObject>();

        foreach (GameObject emitter in emitters)
        {
            if (emitter.GetComponent<IHeatEmitter>().GetHeatEmissionStrength() >= heatSensitivity
                && Vector3.Distance(emitter.transform.position, this.transform.position) < detectionRange)
            {
                newTargets.Add(emitter);
            }
        }

        newTargets.Sort(TargetComparator);

        if (newTargets.Count > maxSimultaneousTargetLimit)
        {
            newTargets.RemoveRange(maxSimultaneousTargetLimit, newTargets.Count - maxSimultaneousTargetLimit);
        }

        return newTargets;
    }

    private List<GameObject> GetHeatEmiitters()
    {
        List<GameObject> emitters = new List<GameObject>();
        var gos = GameObject.FindGameObjectsWithTag("enemy");

        foreach (GameObject go in gos)
        {
            if (go.GetComponent<IHeatEmitter>() != null)
            {
                emitters.Add(go);
            }
        }

        return emitters;
    }

    public float GetDetectionRange()
    {
        return detectionRange;
    }

    public string GetDetectionType()
    {
        return "heat";
    }

    public int GetMaxSimultaneousTargetLimit()
    {
        return maxSimultaneousTargetLimit;
    }

    public void EnableScan()
    {
        scanEnabled = true;
        InvokeRepeating("Detect", 0.0f, scanInterval);
    }

    public void DisableScan()
    {
        scanEnabled = false;
        CancelInvoke("Detect");
    }

    void Detect()
    {
        Debug.Log("Detecting...");
        if (scanEnabled)
        {
            targets = GetDetectedTargets();
        }
    }

    int TargetComparator(GameObject a, GameObject b)
    {
        return Vector3.Distance(a.transform.position, transform.position).CompareTo(Vector3.Distance(b.transform.position, transform.position));
    }
}
