﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RadarSystem : MonoBehaviour
{
    public float effectiveRange = 100000.0f; // m
    public float fieldOfView = 180.0f;  // degrees
    //public float blindHeight = 100.0f; // meter
    public float radarSensitivity = 10.0f;

    public Vector3 forward; // forward directional vector

    public float scanInterval = 1.0f; // seconds

    public bool scanEnabled = false;

    [SerializeField]
    public List<RadarReflectionSystem> targets = new List<RadarReflectionSystem>();

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Scan", 0.0f, scanInterval);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Scan()
    {
        if (scanEnabled)
        {
            List<RadarReflectionSystem> targets = new List<RadarReflectionSystem>();
            var ros = FindObjectsOfType<RadarReflectionSystem>();

            foreach (var ro in ros)
            {
                float distance = Vector3.Distance(ro.transform.position, transform.position);
                float fov = Vector3.Angle(ro.transform.position - transform.position, forward);

                if (distance <= effectiveRange
                    && fov <= fieldOfView
                    && ro.tag == "enemy"
                    && ro.reflectedArea >= radarSensitivity
                    /*&& ro.transform.position.y >= blindHeight*/)
                {
                    targets.Add(ro);
                }
            }

            this.targets = targets;
        }
    }

    public void EnableScan()
    {
        scanEnabled = true;
    }

    public void DisableScan()
    {
        scanEnabled = false;
    }
}
