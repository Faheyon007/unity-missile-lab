﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHeatEmitter
{
    float GetHeatEmissionStrength();    
}
