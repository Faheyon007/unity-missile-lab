﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
class RadarBasedTargetDetectionSystem : MonoBehaviour, ITargetDetectionSystem
{
    public float detectionRange = 10000; // meters
    public int maxSimultaneousTargetLimit = 10;

    public bool scanEnabled = false;
    public float scanInterval = 1.0f;

    /**
     * This is the ability to pick up radar signal.
     * On a scale of 0 - 1 where 1 is the lowest sensitivity.
     * The radar reflection area is  on a scale of 0 - 1 where 1 is high reflectivity.
     * Thus, radar detectors must be very sensitive for objects that have low reflection such as stealth jets.
     */
    public float radarSensitivity = 0.2f;


    public List<GameObject> targets = new List<GameObject>();



    void Start()
    {

    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        
    }


    public List<GameObject> GetDetectedTargets()
    {
        List<GameObject> emitters = GetRadarReflecters();
        List<GameObject> newTargets = new List<GameObject>();

        foreach (GameObject emitter in emitters)
        {
            if (emitter.GetComponent<IRadarReflector>().GetReflection().area >= radarSensitivity
                && Vector3.Distance(emitter.transform.position, this.transform.position) < detectionRange)
            {
                newTargets.Add(emitter);
            }
        }

        newTargets.Sort(TargetComparator);
        
        if(newTargets.Count > maxSimultaneousTargetLimit)
        {
            newTargets.RemoveRange(maxSimultaneousTargetLimit, newTargets.Count - maxSimultaneousTargetLimit);
        }

        return newTargets;
    }

    private List<GameObject> GetRadarReflecters()
    {
        List<GameObject> reflecters = new List<GameObject>();
        var gos = GameObject.FindGameObjectsWithTag("enemy");

        foreach (GameObject go in gos)
        {
            if (go.GetComponent<IRadarReflector>() != null)
            {
                reflecters.Add(go);
            }
        }

        return reflecters;
    }

    public float GetDetectionRange()
    {
        return detectionRange;
    }

    public string GetDetectionType()
    {
        return "radar";
    }

    public int GetMaxSimultaneousTargetLimit()
    {
        return maxSimultaneousTargetLimit;
    }

    public void EnableScan()
    {
        scanEnabled = true;
        InvokeRepeating("Detect", 0.0f, scanInterval);
    }

    public void DisableScan()
    {
        scanEnabled = false;
        CancelInvoke("Detect");
    }

    void Detect()
    {
        Debug.Log("Detecting...");
        if (scanEnabled)
        {
            targets = GetDetectedTargets();
        }
    }

    int TargetComparator(GameObject a, GameObject b)
    {
        return Vector3.Distance(a.transform.position, transform.position).CompareTo( Vector3.Distance(b.transform.position, transform.position));
    }
}


