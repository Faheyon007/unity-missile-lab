﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RadarReflectionSystem : MonoBehaviour
{
    public float reflectedArea = 20.0f; // m^2


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public RadarReflection Reflect()
    {
        return new RadarReflection(reflectedArea, transform);
    }
}
