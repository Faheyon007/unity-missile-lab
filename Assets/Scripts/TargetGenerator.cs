﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class TargetGenerator : MonoBehaviour
{
    public GameObject targetPrefab;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 1000; i++)
        {
            var go = Instantiate(targetPrefab, new Vector3(50, 0, 1000 * i), new Quaternion(0, 0, 0, 0));
            go.name = "T " + i;
            go.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
