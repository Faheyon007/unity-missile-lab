﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RadarReflectorManager : MonoBehaviour
{
    List<IRadarReflector> reflectors = new List<IRadarReflector>();


    // Use this for initialization
    void Start()
    {
        var reflections = GameObject.FindObjectsOfType<RadarReflectionSystem>();
        foreach(var reflection in reflections)
        {
            Debug.Log(reflection.reflectedArea + "\n" + reflection.transform + "\n\n");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
