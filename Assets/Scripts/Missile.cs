﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Missile : MonoBehaviour, IMissile, IHeatEmitter, IRadarReflector
{
    public float heatEmissionStrength = 0.9f;
    public float radarReflectionArea = 0.2f;


    public List<ITargetDetectionSystem> targetDetectionSystems = new List<ITargetDetectionSystem>();


    public float GetHeatEmissionStrength()
    {
        return heatEmissionStrength;
    }

    public RadarReflection GetReflection()
    {
        return new RadarReflection(radarReflectionArea, transform);
    }

    public List<ITargetDetectionSystem> GetTargetDetectionSystems()
    {
        return new List<ITargetDetectionSystem>(gameObject.GetComponents<ITargetDetectionSystem>());
    }

    // Use this for initialization
    void Start()
    {
        targetDetectionSystems = GetTargetDetectionSystems();

        foreach(var tds in targetDetectionSystems)
        {
            if(tds.GetDetectionType() == "radar")
            {
                tds.EnableScan();
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= 6f)
        {
            foreach (var tds in targetDetectionSystems)
            {
                if (tds.GetDetectionType() == "heat")
                {
                    tds.DisableScan();
                }
            }
        }
    }
}
