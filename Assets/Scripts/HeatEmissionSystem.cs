﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class HeatEmissionSystem : MonoBehaviour
{
    public float emissionStrength = 100.0f;

    public HeatEmission EmitHeat()
    {
        return new HeatEmission(emissionStrength, transform);
    }
}
