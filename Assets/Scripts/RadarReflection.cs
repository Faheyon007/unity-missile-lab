﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarReflection
{
    public float area;
    public Transform transform;

    public RadarReflection(float area, Transform transform)
    {
        this.area = area;
        this.transform = transform;
    }
}
