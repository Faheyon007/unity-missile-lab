﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRadarReflector
{
    RadarReflection GetReflection();
}
