﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurbulanceGenerator : MonoBehaviour
{
    // speed limit after which turbulance takes place
    public float speedThreshold = 200.0f;
    public bool turbulanceActive = false;
    public float turbulanceFrequency = 1.5f;    // seconds

    public float forceThreshold = 10.0f;
    public float multiflier = 1000.0f;
    public Vector3 turbulanceProbability = new Vector3(0.7f, 0.75f, 0.2f);

    public Rigidbody rigidbody;


    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        InvokeRepeating("AddTurbulance", 0.0f, turbulanceFrequency);
    }

    // Update is called once per frame
    void Update()
    {

    }


    public Vector3 GenerateTurbulance(Vector3 probability)
    {
        Vector3 force = Vector3.zero;

        force.x = UnityEngine.Random.value < probability.x ? UnityEngine.Random.Range(-forceThreshold, forceThreshold) * multiflier : 0.0f;
        force.y = UnityEngine.Random.value < probability.y ? UnityEngine.Random.Range(-forceThreshold, forceThreshold) * multiflier : 0.0f;
        force.z = UnityEngine.Random.value < probability.z ? UnityEngine.Random.Range(-forceThreshold, forceThreshold) * multiflier : 0.0f;

        return force;
    }

    public Vector3 GenerateTurbulance()
    {
        return GenerateTurbulance(turbulanceProbability);
    }


    public void AddTurbulance()
    {
        if (rigidbody.velocity.magnitude > speedThreshold)
        {
            turbulanceActive = true;
            rigidbody.AddForce(GenerateTurbulance());
            //Debug.Log("Turbulance added.");
        }
        else
        {
            turbulanceActive = false;
        }
    }
}
