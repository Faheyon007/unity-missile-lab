﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MissileLocomotionSystem : MonoBehaviour
{
    public float maxAcceleration = 50.0f;           // m/s2
    public float maxSpeed = 600.0f;                // m/s

    public float currentSpeed;
    public float currentAcceleration;

    public Rigidbody rigidbody;

    public bool ignition = false;


    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        currentAcceleration = maxAcceleration;
        currentSpeed = 0.0f;


        //InvokeRepeating("Thrust", 0.5f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        //Thrust();
    }

    void FixedUpdate()
    {
        if (ignition)
        {
            currentSpeed = Math.Min(currentSpeed + currentAcceleration * Time.deltaTime, maxSpeed);
            rigidbody.velocity = (transform.forward * currentSpeed);
        }
    }

    public void ThrustFixedDelay()
    {
        if (ignition)
        {
            currentSpeed = Math.Min(currentSpeed + currentAcceleration, maxSpeed);
            rigidbody.velocity = (transform.forward * currentSpeed);
        }
    }

    public void Thrust()
    {
        if (ignition)
        {
            currentSpeed = Math.Min(currentSpeed + currentAcceleration * Time.deltaTime, maxSpeed);
            rigidbody.velocity = (transform.forward * currentSpeed);
        }
    }


}
