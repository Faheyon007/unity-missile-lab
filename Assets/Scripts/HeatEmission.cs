﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatEmission
{
    public float strength;
    public Transform transform;


    public HeatEmission(float strength, Transform transform)
    {
        this.strength = strength;
        this.transform = transform;
    }
}
